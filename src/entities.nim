import nico
import vectors
import transforms
import options
import math

const RotationStep* = Tau / 8

type CollisionRectangle* = ref object
  position*: Vector
  dimensions*: Vector


type Faction* = enum
  fIpc
  fEarth


type Entity* = ref object of RootObj
  name*: string
  maxHealth*: int
  health*: int
  maxShields*: int
  shields*: int
  acceleration*: float
  collisionShape*: CollisionRectangle
  position*: Vector
  velocity*: Vector
  rotation*: int # Only 8 directions
  collisionChecked*: bool
  faction*: Faction
  onUpdate*: proc (this: Entity, entities: seq[Entity])
  onCollide*: proc (this: Entity)


method calculateVerteces*(position: Vector, rotation: float, shape: CollisionRectangle): array[4, Vector] {.base.} =
  let shapePosition = rotateVector(shape.position, rotation) + position
  let dimensions = rotateVector(shape.dimensions, rotation)
  let dimensionX = rotateVector(Vector(x: shape.dimensions.x, y: 0), rotation)
  let dimensionY = rotateVector(Vector(x: 0, y: shape.dimensions.y), rotation)

  # Top Left
  result[0] = shapePosition
  # Top Right
  result[1] = shapePosition + dimensionX
  # Bottom Right
  result[2] = shapePosition + dimensions
  # Bottom Left
  result[3] = shapePosition + dimensionY


proc linesColliding*(a0, a1, b0, b1: Vector): Option[Vector] =
  ## This is cursed!
  ## https://jeffreythompson.org/collision-detection/line-line.php
  let uAa = ((b1.x - b0.x) * (a0.y - b0.y) - (b1.y - b0.y) * (a0.x - b0.x))
  let uAb = ((b1.y - b0.y) * (a1.x - a0.x) - (b1.x - b0.x) * (a1.y - a0.y))
  if uAb == 0: # Check if the second part is zero to avoid division by zero, not actually necessary.
    return none(Vector)
  let uA = uAa / uAb

  let uBa = ((a1.x - a0.x) * (a0.y - b0.y) - (a1.y - a0.y) * (a1.x - b0.x))
  let uBb = ((b1.y - b0.y) * (a1.x - a0.x) - (b1.x - b0.x) * (a1.y - a0.y))
  if uBb == 0: # Check if the second part is zero to avoid division by zero, not actually necessary.
    return none(Vector)
  let uB = uBa / uBb

  if uA >= 0 and uA <= 1 and uB >= 0 and uB <= 1:
    result = some(Vector(
                 x: a0.x + (uA * (a1.x - a0.x)),
                 y: a0.y + (uA * (a1.y - a0.y))
                ))


method collide*(a, b: Entity): bool {.base.} =
  let vertecesA = calculateVerteces(a.position, a.rotation.float * RotationStep, a.collisionShape)
  let vertecesB = calculateVerteces(b.position, b.rotation.float * RotationStep, b.collisionShape)

  for i in 0..len(vertecesA) - 1:
    let a0 = vertecesA[i]
    let a1 = if i < len(vertecesA) - 1: vertecesA[i + 1] else: vertecesA[0]
    for j in 0..len(vertecesB) - 1:
      let b0 = vertecesB[j]
      let b1 = if j < len(vertecesB) - 1: vertecesB[j + 1] else: vertecesB[0]
      if linesColliding(a0, a1, b0, b1).isSome:
        return true


proc newEntity*(pos: Vector, faction: Faction, update: proc (this: Entity, entities: seq[Entity])): Entity =
  result = new(Entity)
  result.name = "Entity"
  result.maxHealth = 100
  result.health = result.maxHealth
  result.maxShields = result.maxHealth
  result.shields = result.maxShields
  result.acceleration = 0.25
  result.position = pos
  result.rotation = 0
  result.collisionShape = CollisionRectangle(
                              position: Vector(x: -10, y: -10),
                              dimensions: Vector(x: 20, y: 20)
                            )
  result.faction = faction
  result.onUpdate = update


proc updateFighter*(this: Entity, entities: seq[Entity]) =
  discard

proc updatePlayer*(this: Entity, entities: seq[Entity]) =
  var direction = Vector()
  if btn(pcUp):
    direction.y -= 1
  if btn(pcDown):
    direction.y += 1
  if btn(pcLeft):
    direction.x -= 1
  if btn(pcRight):
    direction.x += 1
  this.velocity = this.velocity + norm(direction) * this.acceleration
  this.position = this.position + this.velocity

  if direction.x == 0 and direction.y == -1:
    this.rotation = 0
  elif direction.x == 1 and direction.y == -1:
    this.rotation = 1
  elif direction.x == 1 and direction.y == 0:
    this.rotation = 2
  elif direction.x == 1 and direction.y == 1:
    this.rotation = 3
  elif direction.x == 0 and direction.y == 1:
    this.rotation = 4
  elif direction.x == -1 and direction.y == 1:
    this.rotation = 5
  elif direction.x == -1 and direction.y == 0:
    this.rotation = 6
  elif direction.x == -1 and direction.y == -1:
    this.rotation = 7
