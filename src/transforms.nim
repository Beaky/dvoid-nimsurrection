import vectors
import math


type Transform =
  array[2, array[2, float]]


proc `*`*(t: Transform, v: Vector): Vector =
  Vector(
        x: v.x * t[0][0] + v.y * t[0][1],
        y: v.x * t[1][0] + v.y * t[1][1]
  )


proc rotationMatrix(phi: float): Transform =
  ## Rotates a vector clockwise
  [[cos(phi), -sin(phi)],
   [sin(phi), cos(phi)]]


proc rotateVector*(v: Vector, phi: float): Vector =
  rotationMatrix(phi) * v
