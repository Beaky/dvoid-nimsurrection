import nico
import ui
import entities
import vectors
import quadtree

const Resolution = [512, 512]
const QuadTreeSize = Vector(x: Resolution[0].float * 32, y: Resolution[0].float * 32)
const PlayerOutOfBoundsMargin = 256

var menuVisible = false
var menuPosition = 0
var qTreePosition = QuadTreeSize * -0.5
var qTree = newQuad(qTreePosition, QuadTreeSize, 0)

var player: Entity
var worldEntities = newSeq[Entity]()


proc menuInput() =
  if btnp(pcBack) or keyp(K_ESCAPE):
    menuVisible = true
  if menuVisible:
    if btnp(pcUp):
      menuPosition = wrap(menuPosition - 1, 3)
    if btnp(pcDown):
      menuPosition = wrap(menuPosition + 1, 3)
    if btnp(pcA) or keyp(K_RETURN):
      case menuPosition:
        of 0:
          menuVisible = false
        of 1:
          discard
        else:
          shutdown()


proc gameInit() =
  loadFont(0, "font.png")
  loadSpriteSheet(0, "ship.png", 32, 32)
  setPalette(loadPalettePico8())
  setSpriteSheet(0)

  player = newEntity(Vector(), fIpc, updatePlayer)
  player.name = "Player"
  player.health = 75

  worldEntities.add(player)
  worldEntities.add(newEntity(Vector(x: -134, y: -134), fEarth, updateFighter))
  for entity in worldEntities:
    discard qTree.insert(entity)


proc gameUpdate(dt: float32) =
  menuInput()
  if menuVisible:
    return

  discard qTree.remove(player)
  discard qTree.insert(player)

  for entity in worldEntities:
    entity.onUpdate(entity, worldEntities)

  for entity in worldEntities:
    if entity.collisionChecked:
      continue
    var encounteredEntities = newSeq[Entity]()
    discard qTree.queryClosebys(entity, encounteredEntities)
    for nearbyEntity in encounteredEntities:
      if nearbyEntity.collisionChecked:
        continue
      if collide(entity, nearbyEntity):
        # This looks weird, but we're accessing the enttiy's onCollide property.
        # Therefore nim can't work it's syntactic sugar
        if entity.onCollide != nil: entity.onCollide(entity)
        if nearbyEntity.onCollide != nil: nearbyEntity.onCollide(nearbyEntity)
    entity.collisionChecked = true


proc gameDraw() =
  cls()
  drawMain(player, worldEntities)
  # Draw Out of bounds message
  let playerBoundsPos = Vector(
                              x: qTreePosition.x + PlayerOutOfBoundsMargin,
                              y: qTreePosition.y + PlayerOutOfBoundsMargin
                              )
  let playerBoundsOuterPos = Vector(
                                   x: qTreePosition.x + QuadTreeSize.x - PlayerOutOfBoundsMargin,
                                   y: qTreePosition.y + QuadTreeSize.y - PlayerOutOfBoundsMargin
                                   )
  if player.position.x < playerBoundsPos.x or player.position.y < playerBoundsPos.y or
     player.position.x > playerBoundsOuterPos.x or player.position.y > playerBoundsOuterPos.y:
    drawPlayerOutOfBounds()


  setColor(11)
  qTree.draw(player.position)

  let verteces = calculateVerteces(player.position, player.rotation.float * 0.7853982, player.collisionShape)
  let circPos = verteces[0].toview(player.position, Vector(x: screenWidth.float, y: screenHeight.float))
  circ(circPos.x, circPos.y, 5)
  setColor(7)
  for i in 0..3:
    let a0 = verteces[i].toView(player.position, Vector(x: screenWidth.float, y: screenHeight.float))
    var a1 = if i < len(verteces) - 1: verteces[i + 1] else: verteces[0]
    a1 = a1.toView(player.position, Vector(x: screenWidth.float, y: screenHeight.float))
    line(a0.x, a0.y, a1.x, a1.y)

  if menuVisible:
    drawMenu(menuPosition)


fixedSize(true)
integerScale(false)
nico.init("dvoid", "ninsurrection")
nico.createWindow("DVOID:NIMSURRECTION", Resolution[0], Resolution[1], 1, true)
nico.run(gameInit, gameUpdate, gameDraw)
