# Package

version = "0.1.0"
author = "Lennard Fleischer"
description = "dvoid_nimsurrection"
license = "GPLv3"

# Deps
requires "nim >= 1.2.0"
requires "nico >= 0.2.5"

srcDir = "src"

task runr, "Runs dvoid_nimsurrection for current platform":
 exec "nim c -r -d:release -o:dvoid_nimsurrection src/main.nim"

task rund, "Runs debug dvoid_nimsurrection for current platform":
 exec "nim c -r -d:debug -o:dvoid_nimsurrection src/main.nim"

task release, "Builds dvoid_nimsurrection for current platform":
 exec "nim c -d:release -o:dvoid_nimsurrection src/main.nim"

task debug, "Builds debug dvoid_nimsurrection for current platform":
 exec "nim c -d:debug -o:dvoid_nimsurrection_debug src/main.nim"

task web, "Builds dvoid_nimsurrection for current web":
 exec "nim js -d:release -o:dvoid_nimsurrection.js src/main.nim"

task webd, "Builds debug dvoid_nimsurrection for current web":
 exec "nim js -d:debug -o:dvoid_nimsurrection.js src/main.nim"

task deps, "Downloads dependencies":
 exec "curl https://www.libsdl.org/release/SDL2-2.0.12-win32-x64.zip -o SDL2_x64.zip"
 exec "unzip SDL2_x64.zip"
 #exec "curl https://www.libsdl.org/release/SDL2-2.0.12-win32-x86.zip -o SDL2_x86.zip"
